/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot00
//--------------------------------------------------------------------------

class Robot00 : public Driver
{
public:
    // Konstruktor
    Robot00()
    {
        // Der Name des Robots
        m_sName = "Robot00";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    //Tutorial1

    con_vec drive(situation& s)
    {
        con_vec result = CON_VEC_EMPTY;

        if( s.starting )
        {
          result.fuel_amount = MAX_FUEL;     // fuel when starting
        }

        if( stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc ) )
        {
          return result;
        }

        result.vc = 20;                      // going slowly
        result.alpha = 0.0;                  // straight
        result.request_pit = 0;              // do not ask to pit

        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot00Instance()
{
    return new Robot00();
}
